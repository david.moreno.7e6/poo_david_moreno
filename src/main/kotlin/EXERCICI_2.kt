import java.util.*

class Beguda(val name: String, var preu: Double, val ensucrada: Boolean){

    init {
        if (ensucrada){
            preu *= 1.1
        }
    }
    fun printData() {
        println("Nom: $name");println("Preu: $preu");println("ensucrada :$ensucrada")
    }
}

fun main(){
    val sc= Scanner(System.`in`)
    println("Introdueix el preu, pes i calories del croissant:")
    val croissant= Pasta("croissant", sc.nextDouble(),sc.nextInt(), sc.nextInt())
    croissant.printData()
    println("Introdueix el preu, pes i calories de l'ensaimada:")
    val ensaimada= Pasta("ensaimada", sc.nextDouble(),sc.nextInt(), sc.nextInt())
    ensaimada.printData()
    println("Introdueix el preu, pes i calories del donut:")
    val donut= Pasta("donut", sc.nextDouble(),sc.nextInt(), sc.nextInt())
    donut.printData()
    val aigua= Beguda("Aigua", 1.00, false)
    val cafeTallat= Beguda("Café Tallat", 1.35, false)
    val teVermell= Beguda("Té Vermell", 1.50, false)
    val cola= Beguda("Cola", 1.65, true)
    aigua.printData()
    cafeTallat.printData()
    teVermell.printData()
    cola.printData()
}