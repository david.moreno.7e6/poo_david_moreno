import java.util.Scanner

class Lampada(var estat: String, var color: String, var intensitat:Int, val id: String){
    val colors= arrayOf("RED", "YELLOW", "BLUE", "GREEN", "PURPLE", "BLACK")
    var lastColor= ""
    init {
        apagar()
    }
    fun apagar(){
        estat= "APAGADA"
        lastColor= color
        color= "BLACK"
        intensitat= 0
        printEstat()
    }
    fun encendre(){
        estat= "ENCESA"
        color= lastColor
        intensitat=1
        printEstat()
    }
    fun changeColor(){
        if (estat== "APAGADA"){
            println("No es pot canviar, está apagada")
        }
        else{
            var newColor= colors.random()
            while (newColor== color){
                newColor= colors.random()
            }
            color= newColor
            printEstat()
        }
    }
    fun changeIntensity(){
        if (estat== "APAGADA"){
            println("No es pot canviar, está apagada")
        }
        else{
            if (intensitat < 5){
                intensitat+= 1
            }
            else {
                intensitat= 1
            }
            printEstat()
        }
    }
    fun printEstat(){
        println("Làmpada $id Estat: $estat Color: $color Intensitat: $intensitat")
    }
}

fun main(){
    val lampada1= Lampada("APAGADA", "RED", 2, "Menjador")
    lampada1.encendre()
    for(i in 1..3){
        lampada1.changeColor()
    }
    while (lampada1.intensitat != 5){
        lampada1.changeIntensity()
    }
    val lampada2= Lampada("APAGADA", "GREEN", 2, "Cuina")
    lampada2.encendre()
    lampada2.changeColor()
    lampada2.changeColor()
    while (lampada2.intensitat != 5){
        lampada2.changeIntensity()
    }
    lampada2.apagar()
    lampada2.encendre()
    lampada2.changeColor()
    while (lampada2.intensitat != 5){
        lampada2.changeIntensity()
    }

}