import java.util.Scanner

class Pasta(val name: String, val preu: Double, val pes: Int, val kcal: Int) {

    fun printData() {
        println("Nom: $name");println("Preu: $preu");println("Pes :$pes");println("Kcal: $kcal")
    }
}

fun main(){
    val sc= Scanner(System.`in`)
    println("Introdueix el preu, pes i calories del croissant:")
    val croissant= Pasta("croissant", sc.nextDouble(),sc.nextInt(), sc.nextInt())
    croissant.printData()
    println("Introdueix el preu, pes i calories de l'ensaimada:")
    val ensaimada= Pasta("ensaimada", sc.nextDouble(),sc.nextInt(), sc.nextInt())
    ensaimada.printData()
    println("Introdueix el preu, pes i calories del donut:")
    val donut= Pasta("donut", sc.nextDouble(),sc.nextInt(), sc.nextInt())
    donut.printData()
}