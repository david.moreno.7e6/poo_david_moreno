
class Brazo(var obertura: Double, var altitud: Double, var encendido: Boolean){
    init {
        obertura= 0.0
        altitud= 0.0
        encendido= false
        println("Obertura: $obertura Altitud: $altitud cm Encendido: $encendido")
    }
    fun power(){
        encendido = !encendido
        println("Obertura: $obertura Altitud: $altitud cm Encendido: $encendido")
    }
    fun obertura(new: Int){
        if (encendido){
            val newObertura= obertura + new
            if (newObertura in 0.0..360.0){
                obertura= newObertura
            }
            else if (newObertura > 360){
                obertura= 360.0
            }
            else{
                obertura= 0.0
            }
        }
        else{
            println("No es put dur a terme amb el braç apagat...")
        }

        println("Obertura: $obertura Altitud: $altitud cm Encendido: $encendido")
    }

    fun altura(new: Int){
        if (encendido){
            val newObertura= altitud + new
            if (newObertura in 0.0..30.0){
                altitud= newObertura
            }
            else if (newObertura > 30){
                altitud= 30.0
            }
            else{
                altitud= 0.0
            }
        }
        else{
            println("No es put dur a terme amb el braç apagat...")
        }
        println("Obertura: $obertura Altitud: $altitud cm Encendido: $encendido")
    }
}

fun main(){
    val braç= Brazo(41.2, 25.0, true )
    braç.power()
    braç.altura(3)
    braç.obertura(180)
    braç.altura(-3)
    braç.obertura(-180)
    braç.altura(3)
    braç.power()
}