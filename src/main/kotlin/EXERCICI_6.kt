open class Electrodomestic(val preuBase: Int, var color: String= "BLANC", var consum: String= "G", val pes: Int= 5){
    open val name= "Electrodomestic"
    val consums= arrayOf("A","B","C","D","E","F","G")
    val colors= arrayOf("BLANC","PLATEJAT","COLOR")
    var preu= preuBase
    init {
        if (color.uppercase() !in colors ){
            color ="BLANC"
        }
        else if(consum.uppercase() !in consums){
            consum ="G"
        }
    }
    open fun preuFinal(){
        when(consum){
            "A"-> preu+=35
            "B"-> preu+=30
            "C"-> preu+=25
            "D"-> preu+=20
            "E"-> preu+=15
            "F"-> preu+=10
        }
        when(pes){
            in 6..20-> preu+=20
            in 20..50-> preu +=50
            in 50..80-> preu+=80
            else -> preu+=100
        }
    }
    open fun factura(){
        println("Preu Base: $preuBase")
        println("Color: $color")
        println("Consum: $consum")
        println("Pes: $pes")
        println("Preu Final: $preu")
    }
}
class Rentadora(val carrega:Int= 5, preu: Int): Electrodomestic(preu){
    var preuRentadora= super.preu
    override val name= "Rentadora"
    override fun preuFinal(){
        when(carrega){
            in 6..7-> preuRentadora +=55
            8-> preuRentadora += 70
            9-> preuRentadora += 85
            10-> preuRentadora +=100
        }
    }
    override fun factura(){
        println("Preu Base: $preu")
        println("Preu Final: $preuRentadora")
    }
}
class Televisor(val tamany: Int = 28,preu: Int): Electrodomestic(preu){
    var preuTv= preu
    override val name= "Televisor"
    override fun preuFinal() {
        when(tamany){
            in 29..32-> preuTv+=50
            in 32..42->preuTv+=100
            in 42..50->preuTv+=150
            else-> preuTv+=200
        }
    }
    override fun factura(){
        println("Preu Base: $preu")
        println("Preu Final: $preuTv")
    }
}

fun main(){
   val electrodomestics= arrayOf(
       Electrodomestic(50,"red", "z", 10),
       Electrodomestic(15,"Platejat","A", 50),
       Electrodomestic(45,"COLOR", "T", 31),
       Electrodomestic(68,"BLANC","B", 82),
       Electrodomestic(78, "AMARILLO", "C",105 ),
       Electrodomestic(150,"GREEN", "D", 24),
       Rentadora(5, Electrodomestic(50,"BLANC", "D", 45).preu),
       Rentadora(9, Electrodomestic(180,"ROJO", "C", 75).preu),
       Televisor(32,Electrodomestic(540,"YELLOW", "A", 87).preu),
       Televisor(57,Electrodomestic(120,"GREY", "B", 62).preu)
   )
    var preuBaseElectrodomestics= 0
    var preuFinalElectrodomestic=0
    var numberOfElectrodomestics=0
    var preuBaseRentadora= 0
    var preuFinalRentadora=0
    var numberOfRentadora=0
    var preuBaseTv= 0
    var preuFinalTv=0
    var numberOfTv=0
    for(i in electrodomestics) {
        if (i is Rentadora) {
            numberOfRentadora++
            preuBaseRentadora += i.preu
            i.preuFinal()
            preuFinalRentadora += i.preuRentadora
            println("${i.name} $numberOfRentadora :")
            i.factura()
        } else if (i is Televisor) {
            numberOfTv++
            i.preuFinal()
            preuBaseTv += i.preu
            preuFinalTv += i.preuTv
            println("${i.name} $numberOfTv :")
            i.factura()
        }
        else {
            numberOfElectrodomestics++
            i.preuFinal()
            preuBaseElectrodomestics += i.preuBase
            preuFinalElectrodomestic += i.preu
            println("${i.name} $numberOfElectrodomestics :")
            i.factura()
        }
    }
    println()
    println("Electrodomestics:")
    println("Preu Base: $preuBaseElectrodomestics")
    println("Preu Final: $preuFinalElectrodomestic")
    println("Rentadores:")
    println("Preu Base: $preuBaseRentadora")
    println("Preu Final: $preuFinalRentadora")
    println("Televisors:")
    println("Preu Base: $preuBaseTv")
    println("Preu Final: $preuFinalTv")

}