import java.util.Scanner

class Taulell(val preuUnitari: Int, val llargada: Int, val amplada: Int){
    var preuFinal=0

    init {
        preuFinal= amplada*llargada*preuUnitari
    }
}
class Llisto(val preuUnitari: Int, llargada: Int){
    var preuFinal=0
    init {
        preuFinal= llargada*preuUnitari
    }
}

fun main(){
    val sc= Scanner(System.`in`)
    println("Introdueix el numero de productes totals:")
    val input= sc.nextInt()
    var productes= input
    var preu=0
    while (productes>0){
        println("Quin producte vols?")
        val producte= sc.next()
        if("Taulell" in producte|| "taulell" in producte){
            println("Introdueix: preu, llargada i amplada")
            val taulell= Taulell(sc.nextInt(),sc.nextInt(),sc.nextInt())
            preu += taulell.preuFinal
            productes--
        }
        else if("Llisto" in producte|| "llisto" in producte){
            println("Introdueix: preu i llargada")
            val llisto= Llisto(sc.nextInt(),sc.nextInt())
            preu += llisto.preuFinal
            productes--
        }
        else{
            println("Producte no válid")
        }
    }
    println("El preu total és: $preu$")
}